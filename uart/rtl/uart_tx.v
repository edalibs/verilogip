module uart_tx
    (
        // global
        clk_i,     // clock
        reset_i,   // reset
        // parallel interface
        cphb_i,    // one less than the num. of cycles per half uart bit
        ready_o,   // ready to transmit another byte
        wr_en_i,   // transmit byte wr_data_i over uart (single cycle pulse)
        wr_data_i, // byte to transmit (valid when wr_en_i is high)
        // serial interface
        txd_o      // uart tx line
    );

    // global
    input  wire        clk_i;     // clock
    input  wire        reset_i;   // reset
    // parallel interface
    input  wire [15:0] cphb_i;    // one less than the num. of cycles per half uart bit
    output wire        ready_o;   // ready to transmit another byte
    input  wire        wr_en_i;   // transmit byte wr_data_i over uart (single cycle pulse)
    input  wire  [7:0] wr_data_i; // byte to transmit (valid when wr_en_i is high)
    // serial interface
    output wire        txd_o;     // uart tx line

    //--------------------------------------------------------------

    // global assignments
    wire        shift_en;
    // cycle counter
    wire        cycle_counter_zero;
    reg  [16:0] cycle_counter_q;
    // bit counter
    wire        bit_counter_zero;
    reg   [3:0] bit_counter_q;
    // tx shift register
    reg   [8:0] tx_reg_q;

    //--------------------------------------------------------------

    //--------------------------------------------------------------
    // global assignments
    //--------------------------------------------------------------
    assign ready_o  = bit_counter_zero;
    assign txd_o    = tx_reg_q[0];
    assign shift_en = cycle_counter_zero & ~bit_counter_zero;

    //--------------------------------------------------------------
    // cycle counter
    //--------------------------------------------------------------
    assign cycle_counter_zero = ~(|cycle_counter_q);
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            cycle_counter_q <= 17'b0;
        end else if (wr_en_i | shift_en) begin
            cycle_counter_q <= { cphb_i, 1'b1 };
        end else if (~bit_counter_zero) begin
            cycle_counter_q <= cycle_counter_q - 17'd1;
        end
    end


    //--------------------------------------------------------------
    // bit counter
    //--------------------------------------------------------------
    assign bit_counter_zero = ~(|bit_counter_q);
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            bit_counter_q <= 4'd0;
        end else if (wr_en_i) begin
            bit_counter_q <= 4'd10;
        end else if (shift_en) begin
            bit_counter_q <= bit_counter_q - 4'd1;
        end
    end


    //--------------------------------------------------------------
    // tx shift register
    //--------------------------------------------------------------
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            tx_reg_q <= 9'b1;
        end else if (wr_en_i) begin
            tx_reg_q <= { wr_data_i, 1'b0 };
        end else if (shift_en) begin
            tx_reg_q <= { 1'b1, tx_reg_q[8:1] };
        end
    end
endmodule
