module uart
    (
        // global
        clk_i,       // clock
        reset_i,     // active high reset
        // interrupt
        interrupt_o, // active high interrupt (synchronous to clk_i)
        // parallel interface
        wr_i,        // write the data wr_data_i to register addr_i
        rd_i,        // read the data in register addr_i (rd_data_o valid next cycle)
        addr_i,      // address of register to be read/written
        wr_data_i,   // data to be written, must be valid when wr_i is high
        rd_data_o,   // data read, valid in the cycle after rd_i was asserted
        // serial interface
        rxd_i,       // uart rx line
        txd_o        // uart tx line
    );

    // global
    input  wire        clk_i;       // clock
    input  wire        reset_i;     // active high reset
    // interrupt
    output wire        interrupt_o; // active high interrupt (synchronous to clk_i)
    // parallel interface
    input  wire        wr_i;        // write the data wr_data_i to register addr_i
    input  wire        rd_i;        // read the data in register addr_i (rd_data_o valid next cycle)
    input  wire  [2:0] addr_i;      // address of register to be read/written
    input  wire [15:0] wr_data_i;   // data to be written, must be valid when wr_i is high
    output reg  [15:0] rd_data_o;   // data read, valid in the cycle after rd_i was asserted
    // serial interface
    input  wire        rxd_i;       // uart rx line
    output wire        txd_o;       // uart tx line

    //--------------------------------------------------------------

    // global assignments
    // read register
    // control registers
    reg  [15:0] rd_data;
    reg         tx_fifo_wr;
    reg         rx_fifo_rd;
    reg  [15:0] cphb_q;
    reg   [4:0] int_en_q;
    reg   [4:0] int_state_q;
    // interrupt logic
    wire  [3:0] status_raw;
    wire  [3:0] status_raw_edge;
    reg   [3:0] status_raw_q;
    // rx fifo
    wire        rx_fifo_empty;
    wire        rx_fifo_full;
    wire  [7:0] rx_fifo_dout;
    // rx
    wire        rx_fifo_wr;
    wire        rx_err_stop;
    wire  [7:0] rx_fifo_din;
    // tx fifo
    wire        tx_fifo_empty;
    wire        tx_fifo_full;
    wire  [7:0] tx_fifo_dout;
    // tx
    wire        tx_ready;

    //--------------------------------------------------------------


    //--------------------------------------------------------------
    // global assignments
    //--------------------------------------------------------------
    assign interrupt_o = |int_state_q;


    //--------------------------------------------------------------
    // read register
    //--------------------------------------------------------------
    always @ (posedge clk_i) begin
        if (rd_i) begin
            rd_data_o <= rd_data;
        end
    end


    //--------------------------------------------------------------
    // control registers
    //--------------------------------------------------------------
    always @ (*) begin
        rd_data    = 16'b0;
        tx_fifo_wr = 1'b0;
        rx_fifo_rd = 1'b0;
        //
        case (addr_i)
            3'd0    : begin // data
                rd_data    = { 8'b0, rx_fifo_dout };
                tx_fifo_wr = wr_i;
                rx_fifo_rd = rd_i;
            end
            3'd1    : begin // cphb
                rd_data = cphb_q;
            end
            3'd2    : begin // status
                rd_data = { 12'b0, status_raw };
            end
            3'd3    : begin // interrupt enable
                rd_data = { 11'b0, int_en_q };
            end
            3'd4    : begin // interrupt state
                rd_data = { 11'b0, int_state_q };
            end
            default : begin
            end
        endcase
    end
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            cphb_q      <= 16'b0;
            int_en_q    <= { 5 {1'b0} };
            int_state_q <= { 5 {1'b0} };
        end else if (wr_i) begin
            case (addr_i)
                3'd1    : cphb_q      <= wr_data_i;
                3'd3    : int_en_q    <= wr_data_i[4:0];
                3'd4    : int_state_q <= wr_data_i[4:0];
                default : begin
                end
            endcase
        end else begin
            int_state_q <= int_state_q | ( { (rx_err_stop & rx_fifo_wr), status_raw_edge } & int_en_q );
        end
    end


    //--------------------------------------------------------------
    // interrupt logic
    //--------------------------------------------------------------
    assign status_raw = { ~rx_fifo_empty, rx_fifo_full,
                          ~tx_fifo_empty, tx_fifo_full };
    assign status_raw_edge = status_raw_q ^ status_raw;
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            status_raw_q <= { 4 {1'b0} };
        end else begin
            status_raw_q <= status_raw;
        end
    end


    //--------------------------------------------------------------
    // rx fifo
    //--------------------------------------------------------------
    fifo
        #(
            .FIFO_PASSTHROUGH (0),
            .FIFO_WIDTH       (8),
            .FIFO_DEPTH_X     (2)
        ) i_rx_fifo (
            // global
            .clk_i     (clk_i),
            .reset_i   (reset_i),
            // control and status
            .flush_i   (1'b0),
            .empty_o   (rx_fifo_empty),
            .full_o    (rx_fifo_full),
            // write port
            .wr_i      (rx_fifo_wr),
            .wr_data_i (rx_fifo_din),
            // read port
            .rd_i      (rx_fifo_rd),
            .rd_data_o (rx_fifo_dout)
        );


    //--------------------------------------------------------------
    // rx
    //--------------------------------------------------------------
    uart_rx i_uart_rx
        (
            // global
            .clk_i         (clk_i),
            .reset_i       (reset_i),
            // parallel interface
            .cphb_i        (cphb_q),
            .rd_strobe_o   (rx_fifo_wr),
            .rd_err_stop_o (rx_err_stop),
            .rd_data_o     (rx_fifo_din),
            // serial interface
            .rxd_i         (rxd_i)
        );


    //--------------------------------------------------------------
    // tx fifo
    //--------------------------------------------------------------
    fifo
        #(
            .FIFO_PASSTHROUGH (0),
            .FIFO_WIDTH       (8),
            .FIFO_DEPTH_X     (2)
        ) i_tx_fifo (
            // global
            .clk_i     (clk_i),
            .reset_i   (reset_i),
            // control and status
            .flush_i   (1'b0),
            .empty_o   (tx_fifo_empty),
            .full_o    (tx_fifo_full),
            // write port
            .wr_i      (tx_fifo_wr),
            .wr_data_i (wr_data_i[7:0]),
            // read port
            .rd_i      (tx_ready & ~tx_fifo_empty),
            .rd_data_o (tx_fifo_dout)
        );


    //--------------------------------------------------------------
    // tx
    //--------------------------------------------------------------
    uart_tx i_uart_tx
        (
            // global
            .clk_i     (clk_i),
            .reset_i   (reset_i),
            // parallel interface
            .cphb_i    (cphb_q),
            .ready_o   (tx_ready),
            .wr_en_i   (tx_ready & ~tx_fifo_empty),
            .wr_data_i (tx_fifo_dout),
            // serial interface
            .txd_o     (txd_o)
        );
endmodule
