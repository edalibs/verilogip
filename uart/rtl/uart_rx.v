module uart_rx
    (
        // global
        clk_i,         // clock
        reset_i,       // active high reset
        // parallel interface
        cphb_i,        // one less than the num. of cycles per half uart bit
        rd_strobe_o,   // rd_data_o is valid this cycle
        rd_err_stop_o, // read error encountered (valid when rd_strobe_o high)
        rd_data_o,     // received data (valid when rd_strobe_o high)
        // serial interface
        rxd_i          // uart rx line
    );

    // global
    input  wire        clk_i;         // clock
    input  wire        reset_i;       // active high reset
    // parallel interface
    input  wire [15:0] cphb_i;        // one less than the num. of cycles per half uart bit
    output wire        rd_strobe_o;   // rd_data_o is valid this cycle
    output wire        rd_err_stop_o; // read error encountered (valid when rd_strobe_o high)
    output wire  [7:0] rd_data_o;     // received data (valid when rd_strobe_o high)
    // serial interface
    input  wire        rxd_i;         // uart rx line

    //--------------------------------------------------------------

    // global assignments
    wire        rxd_start;
    wire        shift_en;
    // synchroniser
    wire        rxd_bit;
    wire        rxd_negedge;
    reg   [2:0] sync_q;
    // cycle counter
    wire        cycle_counter_zero;
    reg  [15:0] cycle_counter_q;
    // bit counter
    wire        bit_counter_zero;
    reg   [4:0] bit_counter_q;
    // rx shift register
    reg   [8:0] rx_reg_q;

    //--------------------------------------------------------------

    /*
     * __       _____________________________________________________
     *   |     |     |     |     |     |     |     |     |     |
     *   | Str |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  | Stp
     *   |_____|_____|_____|_____|_____|_____|_____|_____|_____|
     *      ^     ^     ^     ^     ^     ^     ^     ^     ^     ^
     * 000##0##0##0##0##0##0##0##0##0##0##0##0##0##0##0##0##0##0##000
     * 00 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
     *    '  '  '  '  '  '  '  '  '  '  '  '  '  '  '  '  '  '  '  '
     *
     */

    //--------------------------------------------------------------
    // global assignments
    //--------------------------------------------------------------
    assign rd_strobe_o   = cycle_counter_zero & (bit_counter_q == 5'd1 ? 1'b1 : 1'b0);
    assign rd_err_stop_o = ~rxd_bit;
    assign rd_data_o     = rx_reg_q[8:1];
    assign rxd_start     = rxd_negedge & bit_counter_zero;
    assign shift_en      = cycle_counter_zero & bit_counter_q[0];


    //--------------------------------------------------------------
    // synchroniser
    //--------------------------------------------------------------
    assign rxd_bit     =  sync_q[1];
    assign rxd_negedge = ~sync_q[1] & sync_q[0];
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            sync_q <= { 3 {1'b1} };
        end else begin
            sync_q <= { rxd_i, sync_q[2:1] };
        end
    end


    //--------------------------------------------------------------
    // cycle counter
    //--------------------------------------------------------------
    assign cycle_counter_zero = ~(|cycle_counter_q);
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            cycle_counter_q <= 16'b0;
        end else if (rxd_start | (cycle_counter_zero & ~bit_counter_zero)) begin
            cycle_counter_q <= cphb_i;
        end else if (~bit_counter_zero) begin
            cycle_counter_q <= cycle_counter_q - 16'd1;
        end
    end


    //--------------------------------------------------------------
    // bit counter
    //--------------------------------------------------------------
    assign bit_counter_zero = ~(|bit_counter_q);
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            bit_counter_q <= 5'd0;
        end else if (rxd_start) begin
            bit_counter_q <= 5'd19;
        end else if (cycle_counter_zero & ~bit_counter_zero) begin
            bit_counter_q <= bit_counter_q - 5'd1;
        end
    end


    //--------------------------------------------------------------
    // rx shift register
    //--------------------------------------------------------------
    always @ (posedge clk_i) begin
        if (shift_en) begin
            rx_reg_q <= { rxd_bit, rx_reg_q[8:1] };
        end
    end
endmodule
