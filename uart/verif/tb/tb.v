`timescale 1ns / 10ps

`define SIM_AFTER_POSEDGE_CLK @(posedge clk); #1;
`define SIM_AFTER_NEGEDGE_CLK @(posedge clk); #1;

module tb
    (
    );

    //--------------------------------------------------------------

    // tb tasks
    reg   [7:0] queue[255:0];
    integer     queue_rd_ptr;
    integer     queue_wr_ptr;
    // sequence
    reg         reset;
    //
    reg         tb_dut_wr;
    reg         tb_dut_rd;
    reg   [2:0] tb_dut_addr;
    reg  [15:0] tb_dut_wr_data;
    // generate a clock
    reg         clk;
    integer     seed;
    // dut
    wire [15:0] dut_tb_rd_data;

    //--------------------------------------------------------------

    parameter TIMEOUT = 0;
    parameter SEED    = 1;

    //--------------------------------------------------------------
    // tb tasks
    //--------------------------------------------------------------
    initial begin
        queue_rd_ptr = 0;
        queue_wr_ptr = 0;
    end
    task queue_wr;
        input  reg [7:0] wr_data;
    begin
        queue[queue_wr_ptr] = wr_data;
        queue_wr_ptr = (queue_wr_ptr + 1) % 256;
    end
    endtask
    //
    task queue_rd;
        output  reg [7:0] rd_data;
    begin
        rd_data      = queue[queue_rd_ptr];
        queue_rd_ptr = (queue_rd_ptr + 1) % 256;
    end
    endtask
    //
    task automatic bus_access;
        input  integer        write;
        input  reg      [2:0] addr;
        input  reg     [15:0] wr_data;
        output reg     [15:0] rd_data;
    begin
        tb_dut_wr      = (write ? 1'b1 : 1'b0);
        tb_dut_rd      = (write ? 1'b0 : 1'b1);
        tb_dut_addr    = addr;
        tb_dut_wr_data = wr_data;
        `SIM_AFTER_POSEDGE_CLK
        rd_data   = dut_tb_rd_data;
        tb_dut_wr = 1'b0;
        tb_dut_rd = 1'b0;
    end
    endtask


    //--------------------------------------------------------------
    // sequence
    //--------------------------------------------------------------
    task automatic test;
        reg [15:0] cphb;

        reg [15:0] rd_data;
        reg  [7:0] wr_byte;
        reg  [7:0] rd_byte;
    begin
        reset = 1'b1;
        `SIM_AFTER_POSEDGE_CLK
        `SIM_AFTER_POSEDGE_CLK
        reset = 1'b0;

        cphb = (seed % 100) + 384; // 434 avg.
        $display("CPHB = %d", cphb);
        bus_access(1, 1, cphb, rd_data);

        repeat (500) begin
            bus_access(0, 2, 0, rd_data); // read status register
            while (rd_data[0] == 1'b1) begin // while tx full
                if (rd_data[3] == 1'b1) begin // if rx not empty
                    bus_access(0, 0, 0, rd_data); // read from rx fifo
                    queue_rd(rd_byte);
                    $display("%t : Reading      0x%02x", $time, rd_data[7:0]);
                    if (rd_byte != rd_data[7:0]) begin
                        $error("%t : Data mismatch: got 0x%02x, expected 0x%02x", $time, rd_data[7:0], rd_byte);
                    end
                end
                bus_access(0, 2, 0, rd_data); // read status register
            end

            wr_byte = $random(seed);
            queue_wr(wr_byte);
            $display("%t : Writing 0x%02x", $time, wr_byte[7:0]);
            bus_access(1, 0, wr_byte, rd_data);
        end

        $display("************************SUCCESS*************************");
        $finish();
    end
    endtask


    //--------------------------------------------------------------
    // general setup
    //--------------------------------------------------------------
    initial
    begin
        $display("********************************************************");
        $dumpfile("wave.lxt");
        $dumpvars(0, tb);

        $display("******************* SIMULATION START *******************");
        $display();
        $display();

        if (TIMEOUT) begin
            #(TIMEOUT*1000);
            $display();
            $display();
            $display("*******************   FAIL - TIMEOUT   *******************");
            $display("Time = %0tus.", $time/100000);
            $fatal();
        end
    end


    //--------------------------------------------------------------
    // generate a clock
    //--------------------------------------------------------------
    initial begin
        clk = 1'b0;

        seed = SEED;
        test;
    end
    //
    always begin
        #5ns;
        clk = ~clk;
    end


    //--------------------------------------------------------------
    // dut
    //--------------------------------------------------------------
    assign tb_dut_dat = dut_tb_dat;
    //
    uart i_uart
        (
            // global
            .clk_i       (clk),
            .reset_i     (reset),
            // interrupt
            .interrupt_o (),
            // parallel interface
            .wr_i        (tb_dut_wr),
            .rd_i        (tb_dut_rd),
            .addr_i      (tb_dut_addr),
            .wr_data_i   (tb_dut_wr_data),
            .rd_data_o   (dut_tb_rd_data),
            // serial interface
            .rxd_i       (tb_dut_dat),
            .txd_o       (dut_tb_dat)
        );
endmodule
