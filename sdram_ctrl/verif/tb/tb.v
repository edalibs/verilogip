module tb
    (
    );

    //--------------------------------------------------------------

    // clock and reset gen.
    reg         reset;
    reg         clk;
    // sdram controller (uut)
    wire [11:0] uut_sdram_addr;
    wire [31:0] uut_sdram_dq;
    wire  [1:0] uut_sdram_ba;
    wire  [3:0] uut_sdram_dqm;
    wire        uut_sdram_clk;
    wire        uut_sdram_cke;
    wire        uut_sdram_we_l;
    wire        uut_sdram_ras_l;
    wire        uut_sdram_cs_l;
    wire        uut_sdram_cas_l;
    // sdram

    //--------------------------------------------------------------


    //--------------------------------------------------------------
    // clock and reset gen.
    //--------------------------------------------------------------
    initial begin
        reset = 1'b1;
        #10;
        reset = 1'b0;
    end
    //
    always begin
        clk = 1'b0;
        #5;
        clk = 1'b1;
        #5;
    end


    //--------------------------------------------------------------
    // sdram controller (uut)
    //--------------------------------------------------------------
    sdram_ctrl
        #(
            .TBX_ID_SZ        (16),
            .SDRAM_ARRAY_SZWX (30),
            .SDRAM_ADDR_SZ    (12),
            .SDRAM_BA_SZ      (2),
            .DATA_SZB         (4)
        ) i_sdram_ctrl (
            .clk_i         (clk),
            .reset_i       (reset),
            // tbx interface
            .treqready_o   (),
            .treqvalid_i   (),
            .treqid_i      (),
            .treqaddr_i    (),
            .treqwrite_i   (),
            .treqdata_i    (),
            .trspready_i   (),
            .trspvalid_o   (),
            .trspid_o      (),
            .trspdata_o    (),
            // sdram interface
            .sdram_clk_o   (uut_sdram_clk),
            .sdram_cke_o   (uut_sdram_cke),
            .sdram_cs_l_o  (uut_sdram_cs_l),
            .sdram_we_l_o  (uut_sdram_we_l),
            .sdram_cas_l_o (uut_sdram_cas_l),
            .sdram_ras_l_o (uut_sdram_ras_l),
            .sdram_addr_o  (uut_sdram_addr),
            .sdram_ba_o    (uut_sdram_ba),
            .sdram_dqm_o   (uut_sdram_dqm),
            .sdram_dq_io   (uut_sdram_dq)
        );


    //--------------------------------------------------------------
    // sdram
    //--------------------------------------------------------------
    mt48lc4m32b2 i_mt48lc4m32b2
        (
            .A11    (uut_sdram_addr[11]),
            .A10    (uut_sdram_addr[10]),
            .A9     (uut_sdram_addr[9]),
            .A8     (uut_sdram_addr[8]),
            .A7     (uut_sdram_addr[7]),
            .A6     (uut_sdram_addr[6]),
            .A5     (uut_sdram_addr[5]),
            .A4     (uut_sdram_addr[4]),
            .A3     (uut_sdram_addr[3]),
            .A2     (uut_sdram_addr[2]),
            .A1     (uut_sdram_addr[1]),
            .A0     (uut_sdram_addr[0]),
            .DQ31   (uut_sdram_dq[31]),
            .DQ30   (uut_sdram_dq[30]),
            .DQ29   (uut_sdram_dq[29]),
            .DQ28   (uut_sdram_dq[28]),
            .DQ27   (uut_sdram_dq[27]),
            .DQ26   (uut_sdram_dq[26]),
            .DQ25   (uut_sdram_dq[25]),
            .DQ24   (uut_sdram_dq[24]),
            .DQ23   (uut_sdram_dq[23]),
            .DQ22   (uut_sdram_dq[22]),
            .DQ21   (uut_sdram_dq[21]),
            .DQ20   (uut_sdram_dq[20]),
            .DQ19   (uut_sdram_dq[19]),
            .DQ18   (uut_sdram_dq[18]),
            .DQ17   (uut_sdram_dq[17]),
            .DQ16   (uut_sdram_dq[16]),
            .DQ15   (uut_sdram_dq[15]),
            .DQ14   (uut_sdram_dq[14]),
            .DQ13   (uut_sdram_dq[13]),
            .DQ12   (uut_sdram_dq[12]),
            .DQ11   (uut_sdram_dq[11]),
            .DQ10   (uut_sdram_dq[10]),
            .DQ9    (uut_sdram_dq[9]),
            .DQ8    (uut_sdram_dq[8]),
            .DQ7    (uut_sdram_dq[7]),
            .DQ6    (uut_sdram_dq[6]),
            .DQ5    (uut_sdram_dq[5]),
            .DQ4    (uut_sdram_dq[4]),
            .DQ3    (uut_sdram_dq[3]),
            .DQ2    (uut_sdram_dq[2]),
            .DQ1    (uut_sdram_dq[1]),
            .DQ0    (uut_sdram_dq[0]),
            .BA0    (uut_sdram_ba[0]),
            .BA1    (uut_sdram_ba[1]),
            .DQM3   (uut_sdram_dqm[3]),
            .DQM2   (uut_sdram_dqm[2]),
            .DQM1   (uut_sdram_dqm[1]),
            .DQM0   (uut_sdram_dqm[0]),
            .CLK    (uut_sdram_clk),
            .CKE    (uut_sdram_cke),
            .WENeg  (uut_sdram_we_l),
            .RASNeg (uut_sdram_ras_l),
            .CSNeg  (uut_sdram_cs_l),
            .CASNeg (uut_sdram_cas_l)
        );

endmodule
