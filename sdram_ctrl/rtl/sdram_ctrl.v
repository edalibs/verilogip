module sdram_ctrl
    (
        clk_i,
        reset_i,
        // tbx interface
        treqready_o,
        treqvalid_i,
        treqid_i,
        treqaddr_i,
        treqwrite_i,
        treqdata_i,
        trspready_i,
        trspvalid_o,
        trspid_o,
        trspdata_o,
        // sdram interface
        sdram_clk_o,
        sdram_cke_o,
        sdram_cs_l_o,
        sdram_we_l_o,
        sdram_cas_l_o,
        sdram_ras_l_o,
        sdram_addr_o,
        sdram_ba_o,
        sdram_dqm_o,
        sdram_dq_io
    );

    parameter TBX_ID_SZ        = 1;
    parameter SDRAM_ARRAY_SZWX = 30;
    parameter SDRAM_ADDR_SZ    = 12;
    parameter SDRAM_BA_SZ      = 2;
    parameter DATA_SZB         = 4;

    localparam DATA_SZ = 8*DATA_SZB;

    input  wire                        clk_i;
    input  wire                        reset_i;
    // tbx interface
    output wire                        treqready_o;
    input  wire                        treqvalid_i;
    input  wire        [TBX_ID_SZ-1:0] treqid_i;
    input  wire [SDRAM_ARRAY_SZWX-1:0] treqaddr_i;
    input  wire                        treqwrite_i;
    input  wire          [DATA_SZ-1:0] treqdata_i;
    input  wire                        trspready_i;
    output wire                        trspvalid_o;
    output wire        [TBX_ID_SZ-1:0] trspid_o;
    output wire          [DATA_SZ-1:0] trspdata_o;
    // sdram interface
    output wire                        sdram_clk_o;
    output wire                        sdram_cke_o;
    output wire                        sdram_cs_l_o;
    output wire                        sdram_we_l_o;
    output wire                        sdram_cas_l_o;
    output wire                        sdram_ras_l_o;
    output wire    [SDRAM_ADDR_SZ-1:0] sdram_addr_o;
    output wire      [SDRAM_BA_SZ-1:0] sdram_ba_o;
    output wire         [DATA_SZB-1:0] sdram_dqm_o;
    inout  wire          [DATA_SZ-1:0] sdram_dq_io;

    //--------------------------------------------------------------


    //--------------------------------------------------------------


endmodule
