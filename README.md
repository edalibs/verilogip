# Free Verilog IP

## UART

| Register Name        | Offset        | Bits  | Access Mode | Description |
| -------------------- | -------------:| -----:|:-----------:| ----------- |
| data                 | 0             |   7:0 | RW          | Read this register to read from the UART RX FIFO, write it to write to the UART TX FIFO |
| cphb                 | 1             |  15:0 | RW          | One less than half of the number of clock cycles ber data bit |
| status.rx_empty_l    | 2             |     3 | RO          | RX FIFO not empty |
| status.rx_full       | 2             |     2 | RO          | RX FIFO full      |
| status.tx_empty_l    | 2             |     1 | RO          | TX FIFO not empty |
| status.tx_full       | 2             |     0 | RO          | TX FIFO full      |
| int_en.rx_error      | 3             |     4 | RW          | Enable interrupt on RX error state change      |
| int_en.rx_empty_l    | 3             |     3 | RW          | Enable interrupt on RX FIFO empty state change |
| int_en.rx_full       | 3             |     2 | RW          | Enable interrupt on RX FIFO full state change  |
| int_en.tx_empty_l    | 3             |     1 | RW          | Enable interrupt on TX FIFO empty state change |
| int_en.tx_full       | 3             |     0 | RW          | Enable interrupt on TX FIFO full state change  |
| int_state.rx_error   | 4             |     4 | RW          | RX error interrupt state                       |
| int_state.rx_empty_l | 4             |     3 | RW          | RX FIFO empty interrupt state                  |
| int_state.rx_full    | 4             |     2 | RW          | RX FIFO full interrupt state                   |
| int_state.tx_empty_l | 4             |     1 | RW          | TX FIFO empty interrupt state                  |
| int_state.tx_full    | 4             |     0 | RW          | TX FIFO full interrupt state                   |
