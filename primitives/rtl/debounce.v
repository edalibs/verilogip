module debounce
    (
        clk_i,   // clock
        reset_i, // reset
        //
        btn_i,   // raw button input
        btn_o    // debounced button output
    );

    parameter COUNTER_SZ  = 16;       // debounce counter size
    parameter COUNTER_MAX = 16'd1000; // debounce counter max. value

    input  wire clk_i;   // clock
    input  wire reset_i; // reset
    //
    input  wire btn_i;   // raw button input
    output reg  btn_o;   // debounced button output

    //--------------------------------------------------------------

    wire                  btn_edge;
    reg             [1:0] btn_q;
    reg  [COUNTER_SZ-1:0] counter_q;

    //--------------------------------------------------------------


    assign btn_edge = ^btn_q;
    //
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            btn_q <= 2'b0;
        end else begin
            btn_q <= { btn_i, btn_q[1] };
        end
    end


    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            counter_q <= COUNTER_MAX;
        end else if (btn_edge) begin
            counter_q <= COUNTER_MAX;
        end else if (|counter_q) begin
            counter_q <= counter_q - { { COUNTER_SZ-1 {1'b0} }, 1'b1 };
        end
    end


    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            btn_o <= 1'b0;
        end else if (~(|counter_q)) begin
            btn_o <= btn_q[1];
        end
    end
endmodule
