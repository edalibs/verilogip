module fifo
    (
        // global
        clk_i,     // clock
        reset_i,   // reset
        // control and status
        flush_i,   // flush fifo
        empty_o,   // fifo is empty
        full_o,    // fifo is full
        // write port
        wr_i,      // write the data wr_data_i to fifo
        wr_data_i, // data to write (valid when wr_i is high)
        // read port
        rd_i,      // pop the data present on rd_data_o from the fifo
        rd_data_o  // data currently first on output (valid when fifo not empty)
    );

    parameter FIFO_PASSTHROUGH = 0;         // add bypass logic to send write data round fifo when empty
    parameter FIFO_WIDTH       = 1;         // fifo width in bits
    parameter FIFO_DEPTH_X     = 1;         // base 2 exponent of fifo depth

    // global
    input  wire                  clk_i;     // clock
    input  wire                  reset_i;   // reset
    // control and status
    input  wire                  flush_i;   // flush fifo
    output wire                  empty_o;   // fifo is empty
    output reg                   full_o;    // fifo is full
    // write port
    input  wire                  wr_i;      // write the data wr_data_i to fifo
    input  wire [FIFO_WIDTH-1:0] wr_data_i; // data to write (valid when wr_i is high)
    // read port
    input  wire                  rd_i;      // pop the data present on rd_data_o from the fifo
    output wire [FIFO_WIDTH-1:0] rd_data_o; // data currently first on output (valid when fifo not empty)

    //--------------------------------------------------------------

    localparam FIFO_DEPTH = 2**FIFO_DEPTH_X;

    // interface assignments
    // status signals
    reg                  empty_int;
    // pointers
    reg [FIFO_DEPTH_X:0] rd_ptr_q;
    reg [FIFO_DEPTH_X:0] wr_ptr_q;
    // memory
    reg [FIFO_WIDTH-1:0] mem [FIFO_DEPTH-1:0];

    //--------------------------------------------------------------

    //--------------------------------------------------------------
    // interface assignments
    //--------------------------------------------------------------
    generate if (FIFO_PASSTHROUGH) begin
        assign empty_o    = (empty_int == 1'b1 ? ~wr_i : empty_int);
        assign rd_data_o  = (empty_int == 1'b1 ? wr_data_i : mem[rd_ptr_q[FIFO_DEPTH_X-1:0]]);
    end else begin
        assign empty_o    = empty_int;
        assign rd_data_o  = mem[rd_ptr_q[FIFO_DEPTH_X-1:0]];
    end endgenerate


    //--------------------------------------------------------------
    // status signals
    //--------------------------------------------------------------
    always @ (*) begin
        empty_int = 1'b0;
        full_o    = 1'b0;
        if (rd_ptr_q[FIFO_DEPTH_X-1:0] == wr_ptr_q[FIFO_DEPTH_X-1:0]) begin
            if (rd_ptr_q[FIFO_DEPTH_X] == wr_ptr_q[FIFO_DEPTH_X]) begin
                empty_int = 1'b1;
            end else begin
                full_o = 1'b1;
            end
        end
    end


    //--------------------------------------------------------------
    // pointers
    //--------------------------------------------------------------
    always @ (posedge clk_i or posedge reset_i) begin
        if (reset_i) begin
            rd_ptr_q <= { FIFO_DEPTH_X+1 {1'b0} };
            wr_ptr_q <= { FIFO_DEPTH_X+1 {1'b0} };
        end else begin
            if (flush_i) begin
                rd_ptr_q <= { FIFO_DEPTH_X+1 {1'b0} };
                wr_ptr_q <= { FIFO_DEPTH_X+1 {1'b0} };
            end else begin
                if (rd_i) begin
                    rd_ptr_q <= rd_ptr_q + { { FIFO_DEPTH_X {1'b0} }, 1'b1 };
                end
                if (wr_i) begin
                    wr_ptr_q <= wr_ptr_q + { { FIFO_DEPTH_X {1'b0} }, 1'b1 };
                end
            end
        end
    end


    //--------------------------------------------------------------
    // memory
    //--------------------------------------------------------------
    always @ (posedge clk_i) begin
        if (wr_i) begin
            mem[wr_ptr_q[FIFO_DEPTH_X-1:0]] <= wr_data_i;
        end
    end
endmodule
